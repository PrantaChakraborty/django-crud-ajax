from django.urls import path
from .views import HomePageView, create_student, update_student, delete_student


urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('create_student/', create_student, name='create-student'),
    path('update_student/', update_student, name='update-student'),
    path('delete_student/', delete_student, name='delete-student'),
]