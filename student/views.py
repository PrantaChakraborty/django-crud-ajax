import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.generic import TemplateView, ListView, View
from .models import Student
from django.views.decorators.csrf import csrf_exempt


# Create your views here.


class HomePageView(ListView):
    model = Student
    template_name = 'home.html'
    context_object_name = 'student_list'


@csrf_exempt
def create_student(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        gender = request.POST.get('gender')
        try:
            student = Student(name=name, email=email, gender=gender)
            student.save()
            student_data = {'id': student.id, 'created_at': student.created_at, 'error': False,
                            'errorMessage': 'Student added Successfully'}
            return JsonResponse(student_data, safe=False)
        except:
            student_data = {'error': True, 'errorMessage': 'Failed to Add student'}
            return JsonResponse(student_data, safe=False)


@csrf_exempt
def update_student(request):
    if request.method == 'POST':
        data = request.POST.get('data')
        data_dict = json.loads(data)
        try:
            for d in data_dict:
                student = Student.object.get(id=d['id'])
                student.name = d['name']
                student.email = d['email']
                student.gender = d['gender']
                student.save()
            student_data = {'error': True, 'errorMessage': 'Updated Successfully'}
            return JsonResponse(student_data, safe=False)
        except:
            student_data = {'error': True, 'errorMessage': 'Failed to Update'}
            return JsonResponse(student_data, safe=False)


@csrf_exempt
def delete_student(request):
    id = request.POST.get('id')
    try:
        student = Student.object.get(id=id)
        student.delete()
        student_data = {'error': True, 'errorMessage': 'Deleted Successfully'}
        return JsonResponse(student_data, safe=False)
    except:
        student_data = {'error': True, 'errorMessage': 'Can not delete'}
        return JsonResponse(student_data, safe=False)

